from flask import Blueprint, jsonify, request 
from models.user import User  
from app import db
from database.database import db_session
import json
from flask_jwt_extended import jwt_required , get_jwt_identity
# from flask_jwt_extended import jwt_required, current_user, get_current_user, get_jwt_identity

user = Blueprint('user', __name__) 

@user.route('/user/')
@jwt_required()
def user_list():
    users = User.query.all() 
    list_data = [user.obj_to_dict() for user in users]
    return jsonify(list_data)

@user.route('/user/logged/')
@jwt_required()
def add_about(): 
    result = User.query.filter_by(email=get_jwt_identity()).first()
    res = jsonify(result.obj_to_dict()), 200
    return res

@user.route('/user/register/', methods=['POST'])
def user_register(): 
    email = request.form['email'] 
    test = User.query.filter_by(email=email).first()
    if test:
        return jsonify(message='That email already exists'), 409
    else: 
        first_name = request.form['first_name'] 
        last_name = request.form['last_name'] 
        password = request.form['password']  
        role = request.form.get('role')  
        if not role:
            return jsonify(message='Role is necesary'), 409  
        user = User(first_name=first_name, last_name=last_name, email=email, password=password, role=role)
        db_session.add(user)
        db_session.commit()
        return jsonify(message='User created successfully'), 201 


def strtobool (val): 
    val = val.lower()
    if val in ('y', 'yes', 't', 'true', 'on', '1'):
        return True
    elif val in ('n', 'no', 'f', 'false', 'off', '0'):
        return False
    else:
        raise ValueError("invalid truth value %r" % (val,))