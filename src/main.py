
from database.database import engine 
from pathlib import Path
import pandas as pd
from models.vehicle import Vehicle  
import app as create_app 

from views.vehicle import vehicle
from views.user import user
from views.auth import auth

BASE_DIR = Path(__file__).resolve().parent.parent
DATABASE = Path(f'{BASE_DIR}').joinpath('data.sqlite3').is_file()

if DATABASE:
    print('existe una base de datos')
else:
    print('no existe')
    from database.database import init_db
    init_db()
    #feed data from CSV
    file_name = 'many_bus.csv'
    df = pd.read_csv(f'{BASE_DIR}/src/utils/{file_name}')
    print(df)
    df.to_sql(con=engine, index_label='id', name=Vehicle.__tablename__, if_exists='replace')

#--------------------------------------------------
#   App
#-------------------------------------------------- 
app = create_app.app

PORT = 3200
HOST = '0.0.0.0'

with app.app_context():
    app.register_blueprint(vehicle)
    app.register_blueprint(user)
    app.register_blueprint(auth) 

if __name__ == '__main__':  
    app.run(host=HOST, port=PORT)
    print(f'Server running in port:  {PORT}') 