#### REST API MANY BUS HELGEN - COMPLEMENT TO THE TECHNICAL CHALLENGE:

##### Description

The project is developed with FLASK, it contains the models defined with SQLALCHEMY coupled with SQLITE:
- user
- vehicle 

The *utils* folder contains the csv files that were provided for the technical challenge, which were imported into the data.sqlite3 database.

The database folder contains the database session configuration with the *SQLALCHEMY* library.

The *view* folder contains the *REST* methods for each model, auth, user, vehicle.

the app file contains the application configuration so as not to cause import redundancies, and the *main* file is the main one to run.

The application runs on port 3200
It contains 3 users with different roles:
- superuser
- operator
- user

Authenticates with the post method to the /login endpoint
account with jwt however the password is not encrypted

users and the full list of many_bus loaded into the database

**users:**

email = operator@asd.com
password = qweasdfg

email = superuser@asd.com
password = qweasdfg

email = user@asd.com
password = qweasdfg


##### Requirements:
1. Python 3 https://www.python.org/downloads/
3. Virtual Environment ```pip3 install virtualenv```  
  
Run the following command to create the virtual environment:

```
$ python3 -m venv env
```

Run created virtual environment, then install project dependencies:
The *requirements.txt* file is in the **root**

```
$ pip3 install -r requirements.txt
$ python3 -m pip3 install --upgrade pip
```

Once the installation is complete, we activate the virtual environment and run the main.py file with the following command:

```
$ python3 src/main.py
```
