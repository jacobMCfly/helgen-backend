from flask import Blueprint, jsonify, request 
from models.user import User
from flask_jwt_extended import create_access_token

auth = Blueprint('auth', __name__)

@auth.route('/auth/')
def index():
    return "Hello, auth!"

@auth.route('/login/', methods=['POST'])  
def login():
    if request.is_json:
        email = request.json['email']
        password = request.json['password']
    else: 
        email = request.form['email']
        password = request.form['password'] 

    test = User.query.filter_by(email=email, password=password).first()
    if test: 
        access_token = create_access_token(identity=email)
        return jsonify(access_token=access_token)
    else:
        return jsonify('Bad email or Password'), 401