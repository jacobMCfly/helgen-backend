from datetime import timedelta
from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from database.database import db_session 
from pathlib import Path
from flask_jwt_extended import JWTManager
from flask_marshmallow import Marshmallow

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///data.sqlite3' 
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False  
app.config['JWT_SECRET_KEY'] = 'helgen-rocks!'
app.config["JWT_ACCESS_TOKEN_EXPIRES"] = timedelta(hours=24,minutes=0,seconds=0)

jwt = JWTManager(app)
db = SQLAlchemy(app) 
ma = Marshmallow(app)
CORS(app, resources={r"*": {"origins": "*"}})

@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()

@app.route('/')
def home():
    return ''
