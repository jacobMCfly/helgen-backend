from flask import Blueprint
from models.vehicle import Vehicle
from flask import Blueprint, jsonify 
from database.database import db_session
from flask_jwt_extended import jwt_required 

vehicle = Blueprint('vehicle', __name__)

@vehicle.route('/vehicle/')
@jwt_required()
def vehicle_list():
    vehicles = db_session.query(Vehicle).distinct(Vehicle.VehicleID).group_by(Vehicle.VehicleID)
    list_data = [vehicle.obj_to_dict() for vehicle in vehicles]
    return jsonify(list_data)

@vehicle.route('/vehicle/<VehicleID>/', methods=['GET'])
@jwt_required()
def vehicle_object(VehicleID):
    if VehicleID:
        try:
            result = Vehicle.query.filter(Vehicle.VehicleID == VehicleID)
            list_data = [vehicle.obj_to_dict() for vehicle in result] 
            res = jsonify(list_data), 200
        except: 
            res = jsonify({'message': 'Error en la busqueda'}), 400
        finally:
            return res 
    res = jsonify({'message': 'id no encontrado'}), 400
    return res
