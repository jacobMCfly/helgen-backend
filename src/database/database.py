from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
import os
from pathlib import Path 


BASE_DIR = Path(__file__).resolve().parent.parent

engine = create_engine(f'sqlite:///data.sqlite3')
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=True,
                                         bind=engine))
Base = declarative_base()
Base.query = db_session.query_property() 

def init_db():
    import models.vehicle
    import models.user
    Base.metadata.create_all(bind=engine)
    