import datetime
from xmlrpc.client import DateTime
from sqlalchemy import Column, Integer, String, Date, Time, Text
from database.database import Base
from datetime import datetime

class Vehicle(Base):
    __tablename__ = 'vehicle'
    id = Column(Integer, primary_key=True, nullable=False, autoincrement=True) 
    DataEntryID = Column(Integer, autoincrement=True, nullable=False) 
    VehicleID = Column(Integer, unique=True, nullable=False) 
    Date = Column(Date, default=datetime.now().strftime('%Y%m%d%'))
    Time = Column(Time, default=datetime.now().strftime('%H%M%S'))
    Latitude = Column(String(120))
    Longitude = Column(String(120))

    def __init__(self, name=None, email=None):
        self.name = name
        self.email = email

    def __repr__(self):
        return f'<User {self.name!r}>'
    
    def obj_to_dict(self):  # for build json format
        return {
            "id": self.id,
            "DataEntryID": self.DataEntryID,
            "VehicleID": self.VehicleID,
            "Date": self.Date, 
            "Time": str(self.Time), 
            "Latitude": self.Latitude, 
            "Longitude": self.Longitude, 
        }
    
