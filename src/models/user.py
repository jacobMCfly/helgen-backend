from sqlalchemy import Column, Integer, String, Boolean
from database.database import Base 
import sqlalchemy.types as types
 
ROLS = {
    'superuser': 'superuser',
    'operator': 'operator',
    'user': 'user'
}

class ChoiceType(types.TypeDecorator):

    impl = types.String

    def __init__(self, choices, **kw):
        self.choices = dict(choices)
        super(ChoiceType, self).__init__(**kw)

    def process_bind_param(self, value, dialect):
        return [k for k, v in self.choices.items() if v == value][0]

    def process_result_value(self, value, dialect):
        return self.choices[value]

class User(Base):
    __tablename__ = 'user' 

    id = Column(Integer, primary_key=True, autoincrement=True)
    first_name = Column(String)
    last_name = Column(String)
    email = Column(String, unique=True)
    password = Column(String)
    role =  Column(ChoiceType(ROLS), nullable=False)
    
    def obj_to_dict(self):  # for build json format
        return {
            "id": self.id,
            "first_name": self.first_name,
            "last_name": self.last_name,
            "email": self.email, 
            "role": self.role, 
        }
    
